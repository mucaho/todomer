:- module main.

:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.

:- implementation.
:- import_module list.
:- import_module concurrent_stream.
:- import_module stream.

main(!IO) :-
    concurrent_stream.init(Stream : concurrent_stream(int), "TestStream", !IO),
    io.write_string("New stream name: ", !IO),
    stream.name(Stream, Name, !IO),
    io.write_string(Name, !IO),
    io.nl(!IO),

    I = 1,
    io.write_string("Putting ", !IO),
    io.write_int(I, !IO),
    io.write_string(" into stream.\n", !IO),
    IUnit = value(1),
    put(Stream, IUnit, !IO),

    io.write_string("Getting ", !IO),
    take(Stream, Result, !IO),
    (
        Result = ok(OUnit),
        OUnit = value(O),
        io.write_int(O, !IO),
        io.write_string(" from stream.\n", !IO)
    ;
        Result = error(Message),
        io.write_string("error: ", !IO),
        io.write_string(error_message(Message), !IO),
        io.nl(!IO)
    ;
        Result = eof,
        io.write_string("EOF.\n", !IO)
    ),

    io.nl(!IO).
