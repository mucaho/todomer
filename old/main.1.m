:- module main.

:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.

:- implementation.
:- import_module thread.
:- import_module thread.channel.

main(!IO) :-
    channel.init(Channel, !IO),
    io.write_string("New channel created.", !IO),
    io.nl(!IO),

    I = 1,
    io.write_string("Putting ", !IO),
    io.write_int(I, !IO),
    io.write_string(" into channel.\n", !IO),
    channel.put(Channel, I, !IO),

    io.write_string("Getting ", !IO),
    channel.take(Channel, O, !IO),
    io.write_int(O, !IO),
    io.write_string(" from channel.\n", !IO),

    io.nl(!IO).
