:- module concurrent_stream.


:- interface.

:- import_module stream.
:- import_module io.

:- type unit(T) ---> value(T).

:- type concurrent_stream(T).

% TODO convert string error to generic error typeclass

:- instance error(string).

:- instance stream(concurrent_stream(T), io).
:- instance input(concurrent_stream(T), io).
:- instance reader(concurrent_stream(T), unit(T), io, string).
:- instance output(concurrent_stream(T), io).
:- instance writer(concurrent_stream(T), unit(T), io).

:- pred init(concurrent_stream(T)::out, string::in, io::di, io::uo) is det.

:- pred add(concurrent_stream(T)::in, unit(T)::in, io::di, io::uo) is det.

:- pred error(concurrent_stream(T)::in, string::in, io::di, io::uo) is det.

:- pred take(concurrent_stream(T)::in, result(unit(T), string)::out, io::di, io::uo) is det.

:- pred close(concurrent_stream(T)::in, io::di, io::uo) is det.


:- implementation.

:- pragma require_feature_set([ concurrency ]).

:- import_module queue.
:- import_module thread.
:- import_module thread.semaphore.
:- import_module store.
:- import_module exception.

:- type concurrent_stream(T) ---> concurrent_stream(
    name, % for stream typeclass
    semaphore, % semaphore that blocks on empty queue read
    semaphore, % mutex that blocks access to critical section
    io_mutvar(queue(result(unit(T), string))) % additional indirection to be able to read latest version of internal queue after lock acquired
).

init(Stream, Name, !IO) :-
    semaphore.init(Semaphore, !IO),
    semaphore.init(Mutex, !IO),
    semaphore.signal(Mutex, !IO),
    queue.init(Queue),
    store.new_mutvar(Queue, QueueVar, !IO),
    Stream = concurrent_stream(Name, Semaphore, Mutex, QueueVar).

add(Stream, Unit, !IO):-
    Stream = concurrent_stream(_, Semaphore, Mutex, QueueVar),
    semaphore.wait(Mutex, !IO),
    store.get_mutvar(QueueVar, QueueIn, !IO),
    queue.put(ok(Unit), QueueIn, QueueOut),
    store.set_mutvar(QueueVar, QueueOut, !IO),
    semaphore.signal(Mutex, !IO),
    semaphore.signal(Semaphore, !IO).

error(Stream, Message, !IO):-
    Stream = concurrent_stream(_, Semaphore, Mutex, QueueVar),
    semaphore.wait(Mutex, !IO),
    store.get_mutvar(QueueVar, QueueIn, !IO),
    queue.put(error(Message), QueueIn, QueueOut),
    store.set_mutvar(QueueVar, QueueOut, !IO),
    semaphore.signal(Mutex, !IO),
    semaphore.signal(Semaphore, !IO).

take(Stream, Result, !IO):-
    Stream = concurrent_stream(_, Semaphore, Mutex, QueueVar),
    semaphore.wait(Semaphore, !IO),
    semaphore.wait(Mutex, !IO),
    store.get_mutvar(QueueVar, QueueIn, !IO),
    (
        if queue.get(ResultTemp, QueueIn, QueueOutTemp) then
            Result = ResultTemp,
            QueueOut = QueueOutTemp
        else
            exception.throw("Internal fatal error occurred!")
    ),
    store.set_mutvar(QueueVar, QueueOut, !IO),
    semaphore.signal(Mutex, !IO).

close(Stream, !IO):-
    Stream = concurrent_stream(_, Semaphore, Mutex, QueueVar),
    semaphore.wait(Mutex, !IO),
    store.get_mutvar(QueueVar, QueueIn, !IO),
    queue.put(eof, QueueIn, QueueOut),
    store.set_mutvar(QueueVar, QueueOut, !IO),
    semaphore.signal(Mutex, !IO),
    semaphore.signal(Semaphore, !IO).

:- instance error(string) where [
    error_message(Error) = Error
].

:- instance stream(concurrent_stream(T), io) where [
    name(Stream, Name, !IO) :-
        Stream = concurrent_stream(Name, _, _, _)
].

:- instance input(concurrent_stream(T), io) where [
].

:- instance reader(concurrent_stream(T), unit(T), io, string) where [
    pred(get/4) is take
].

:- instance output(concurrent_stream(T), io) where [
    flush(_, !IO)
].

:- instance writer(concurrent_stream(T), unit(T), io) where [
    pred(put/4) is add
].
