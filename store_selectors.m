:- module store_selectors.

:- interface.

:- import_module store_state.
:- import_module list.
:- import_module maybe.
:- import_module bool.

% TODO make selectors semidet and fail if NewValue == OldValue
% instead of accepting old and current state
% return lazy(closured, combined higher-order inst/mode/type pred)
% that fails (or returns maybe-no) if state did not change

:- func state(S) ^ todo_list = list(todo).

:- func state(S) ^ selected_todo = maybe(todo).

:- func state(S) ^ active_todo_count = int.

:- func state(S) ^ all_todos_completed = bool.

:- func state(S) ^ any_todos_completed = bool.

:- implementation.

:- import_module store_state.
:- import_module list.
:- import_module bool.
:- import_module int.

State ^ todo_list = State ^ todos.

State ^ selected_todo = MaybeSelectedTodo :-
    MaybeFocusedTodoId = State ^ focused_todo_id,
    (
        MaybeFocusedTodoId = yes(UUID),
        MatchesUUID = (
            pred(Todo::in) is semidet :-
                if Todo ^ id = UUID then true else false
        ),
        (if list.find_first_match(MatchesUUID, State ^ todos, FocusedTodo) then
            MaybeSelectedTodo = yes(FocusedTodo)
        else
            MaybeSelectedTodo = no
        )
    ;
        MaybeFocusedTodoId = no,
        MaybeSelectedTodo = no
    ).

State ^ active_todo_count = Count :-
    Acc = (func(Todo, Sum) = (if Todo ^ completed = no then Sum + 1 else Sum)),
    Count = list.foldl(Acc, State ^ todos, 0).

State ^ all_todos_completed = AllCompleted :-
    IsCompleted = ( pred(Todo::in) is semidet :-
        if Todo ^ completed = yes then true else false
    ),
    (if list.all_true(IsCompleted, State ^ todos) then
        AllCompleted = yes
    else
        AllCompleted = no).

State ^ any_todos_completed = AnyCompleted :-
    IsCompleted = ( pred(Todo::in) is semidet :-
        if Todo ^ completed = yes then true else false
    ),
    (if list.all_false(IsCompleted, State ^ todos) then
        AnyCompleted = no
    else
        AnyCompleted = yes).
