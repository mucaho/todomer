:- module store_reducer.

:- interface.

:- import_module globals.
:- import_module store_actions.

:- instance reducer_action(create_todo).
:- instance reducer_action(delete_todo).
:- instance reducer_action(focus_todo).
:- instance reducer_action(update_todo_text).
:- instance reducer_action(update_todo_completed).
:- instance reducer_action(udpate_all_completed).
:- instance reducer_action(clear_all_completed).
:- instance reducer_action(refresh_component).

:- implementation.

:- import_module store_state.
:- import_module list.
:- import_module bool.
:- import_module maybe.
:- import_module builtin.

:- instance reducer_action(create_todo) where [(
    reduce(Action, !State) :-
        Action = create_todo(Text),
        uuid(UUID),
        NewTodo = todo(UUID, Text, no),
        !State ^ todos := [NewTodo | !.State ^ todos]
)].

:- instance reducer_action(delete_todo) where [(
    reduce(Action, !State) :-
        Action = delete_todo(UUID),
        NotMatchesUUID = (
            pred(Todo::in) is semidet :-
                if Todo ^ id = UUID then false else true
        ),
        !State ^ todos := list.filter(NotMatchesUUID, !.State ^ todos)
        % TODO remove obsolete focused uuid
)].

:- instance reducer_action(focus_todo) where [(
    reduce(Action, !State) :-
        Action = focus_todo(MaybeUUID),
        (
            MaybeUUID = yes(UUID),
            MatchesUUID = (
                pred(Todo::in) is semidet :-
                    if Todo ^ id = UUID then true else false
            ),
            (if list.find_first_match(MatchesUUID, !.State ^ todos, FocusedTodo) then
                !State ^ focused_todo_id := yes(FocusedTodo ^ id)
            else
                true
            )
        ;
            MaybeUUID = no,
            !State ^ focused_todo_id := no
        )
)].

:- instance reducer_action(update_todo_text) where [(
    reduce(Action, !State) :-
        Action = update_todo_text(UUID, Text),
        MatchesUUID = (
            pred(Todo::in) is semidet :-
                if Todo ^ id = UUID then true else false
        ),
        (if list.find_first_match(MatchesUUID, !.State ^ todos, OldTodo) then
            NewTodo = OldTodo ^ text := Text,
            (if list.replace_first(!.State ^ todos, OldTodo, NewTodo, NewTodos) then
                !State ^ todos := NewTodos
            else
                true
            )
        else
            true
        )
)].

:- instance reducer_action(update_todo_completed) where [(
    reduce(Action, !State) :-
        Action = update_todo_completed(UUID, Completed),
        MatchesUUID = (
            pred(Todo::in) is semidet :-
                if Todo ^ id = UUID then true else false
        ),
        (if list.find_first_match(MatchesUUID, !.State ^ todos, OldTodo) then
            NewTodo = OldTodo ^ completed := Completed,
            (if list.replace_first(!.State ^ todos, OldTodo, NewTodo, NewTodos) then
                !State ^ todos := NewTodos
            else
                true
            )
        else
            true
        )
)].

:- instance reducer_action(udpate_all_completed) where [(
    reduce(Action, !State) :-
        Action = udpate_all_completed(Completed),
        UpdateCompletion = (func(Todo) = (Todo ^ completed := Completed)),
        !State ^ todos := list.map(UpdateCompletion, !.State ^ todos)
)].

:- instance reducer_action(clear_all_completed) where [(
    reduce(_, !State) :-
        IsNotCompleted = (
            pred(Todo::in) is semidet :-
                if Todo ^ completed = no then true else false
        ),
        !State ^ todos := list.filter(IsNotCompleted, !.State ^ todos)
        % TODO remove obsolete focused uuid
)].

:- instance reducer_action(refresh_component) where [(
    reduce(_, !State) :- !:State = !.State
)].
