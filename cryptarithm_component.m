:- module cryptarithm_component.

:- interface.

:- import_module globals, cryptarithm_view.

:- type cryptarithm ---> cryptarithm(int, int, int).

:- type cryptarithm_component ---> cryptarithm_component(cryptarithm :: cryptarithm, input_line :: string, fail_count :: int).

:- type cryptarithm_component_event ---> completed ; failed; aborted.

:- instance component(cryptarithm_component, cryptarithm_component_event, cryptarithm_view, cryptarithm_view_event).

:- implementation.

:- import_module cryptarithm.
:- import_module string, int, list, io, thread, thread.mvar, exception, solutions.

:- pred generate_cryptarithm(cryptarithm::out, io::di, io::uo) is det.

generate_cryptarithm(Cryptarithm, !IO) :-
    Solutions = cryptarithms,
    list.length(Solutions, SolutionCount),
    % we do not care about modyfing the random number supply destructively
    promise_pure impure RandomIndex = impure_random(SolutionCount),
    RandomSolution = list.det_index0(Solutions, RandomIndex),
    RandomSolution = {X, Y, Z},
    Cryptarithm = cryptarithm(X, Y, Z).

:- instance component(cryptarithm_component, cryptarithm_component_event, cryptarithm_view, cryptarithm_view_event) where [
    (component_initial_mvar(ComponentStateMVar, !IO) :-
        generate_cryptarithm(Cryptarithm, !IO),
        InitialComponentState = cryptarithm_component(Cryptarithm, "", 0),
        view_initial(InitialViewState),
        mvar.init(ComponentStateMVar, !IO),
        mvar.put(ComponentStateMVar, {InitialComponentState, InitialViewState}, !IO)
    ),
    (component_output(ComponentStateMVar, !IO) :- some [!Component, !View] (
        % take previous component state
        mvar.take(ComponentStateMVar, {!:Component, _}, !IO),

        !.Component = cryptarithm_component(Cryptarithm, _, FailCount),
        Cryptarithm = cryptarithm(_, _, Z),
        % update view output
        !:View = cryptarithm_view(Z, FailCount),
        view_output(!.View, ViewLines),
        write_lines(ViewLines, !IO),

        % publish new component state
        mvar.put(ComponentStateMVar, {!.Component, !.View}, !IO)
    )),
    (component_input(Outputs, ComponentStateMVar, !IO) :- some [!Component, !View] (
        % take previous component state
        mvar.take(ComponentStateMVar, {!:Component, !:View}, !IO),

        !.Component = cryptarithm_component(Cryptarithm, Line, _),
        Cryptarithm = cryptarithm(_, _, Z),

        % update view input
        view_input(!.View, string.strip(Line), Events),

        % publish events as component outputs
        EventToOutputsMap = (func(Event) = TempOutputs :-
            (
                Event = submit(GuessedX, GuessedY),
                (if all_digits(
                    (GuessedX / 100) mod 10, (GuessedX / 10) mod 10, GuessedX mod 10,
                    (GuessedY / 100) mod 10, (GuessedY / 10) mod 10, GuessedY mod 10,
                    (Z / 100) mod 10, (Z / 10) mod 10, Z mod 10) then

                    TempOutputs = [ completed ]
                else
                    TempOutputs = [ failed ]
                )
            ;
                Event = cancel,
                TempOutputs = [ aborted ]
            )
        ),
        NestedOutputs = list.map(EventToOutputsMap, Events),
        Outputs = list.condense(NestedOutputs),

        % update component state from events
        EventToNewComponentMap = (pred(Event::in, CurrentComponentState::in, NextComponentState::out) is det :-
            (
                Event = submit(GuessedX, GuessedY),
                (if all_digits(
                    (GuessedX / 100) mod 10, (GuessedX / 10) mod 10, GuessedX mod 10,
                    (GuessedY / 100) mod 10, (GuessedY / 10) mod 10, GuessedY mod 10,
                    (Z / 100) mod 10, (Z / 10) mod 10, Z mod 10) then

                    NextComponentState = (CurrentComponentState ^ fail_count := 0)
                else
                    NextComponentState = (CurrentComponentState ^ fail_count := CurrentComponentState ^ fail_count + 1)
                )
            ;
                Event = cancel,
                NextComponentState = (CurrentComponentState ^ fail_count := 0)
            )
        ),
        list.foldl(EventToNewComponentMap, Events, !Component),

        % publish new component state
        mvar.put(ComponentStateMVar, {!.Component, !.View}, !IO)
    ))
].
