:- module store_thread.

:- interface.

:- import_module globals.
:- import_module store_state.
:- import_module thread.
:- import_module thread.channel.
:- import_module thread.mvar.
:- import_module io.

:- some[S] pred init_store_mvar(mvar(state(S))::out, io::di, io::uo) is det.

:- pred init_action_channel(channel(some_reducer_action)::out, io::di, io::uo) is det.

:- pred execute_store_thread(channel(some_reducer_action)::in, mvar(state(S))::in, state(S)::in, io::di, io::uo) is det.

:- implementation.

:- pragma require_feature_set([ concurrency ]).

:- import_module globals.
:- import_module store_state.
:- import_module thread.
:- import_module thread.channel.
:- import_module thread.mvar.

init_store_mvar(StateMVar, !IO) :-
    InitialState = initial_state,
    mvar.init(StateMVar, !IO),
    mvar.put(StateMVar, InitialState, !IO).

init_action_channel(ActionChannel, !IO) :-
    channel.init(ActionChannel, !IO).

execute_store_thread(ActionChannel, StateMVar, CurrentState, !IO) :-
    thread.yield(!IO),
    channel.take(ActionChannel, some_reducer_action(Action), !IO),

    %trace [
    %    io(!TraceIO)
    %] (
    %    io.write_string("execute_store_thread: received action\n", !TraceIO),
    %    io.write(Action, !TraceIO),
    %    io.nl(!TraceIO)
    %),
    reduce(Action, CurrentState, NextState),
    %trace [
    %    io(!TraceIO)
    %] (
    %    io.write_string("execute_store_thread: new state\n", !TraceIO),
    %    io.write(NextState, !TraceIO),
    %    io.nl(!TraceIO)
    %),

    mvar.put(StateMVar, NextState, !IO),
    execute_store_thread(ActionChannel, StateMVar, NextState, !IO).
