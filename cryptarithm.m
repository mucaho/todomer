:- module cryptarithm.

:- interface.

:- import_module list.

:- func cryptarithms = list({int, int, int}).

:- pred all_digits(
    int, int, int,
    int, int, int,
    int, int, int
).
:- mode all_digits(
    in, in, in,
    in, in, in,
    in, in, in
) is semidet.
:- mode all_digits(
    out, out, out,
    out, out, out,
    out, out, out
) is nondet.
:- mode all_digits(
    out, out, out,
    out, out, out,
    in, in, in
) is nondet.
:- mode all_digits(
    in, in, in,
    in, in, in,
    out, out, out
) is nondet.

:- pred all_sums(int, int, int).
:- mode all_sums(in, in, in) is semidet.
:- mode all_sums(in, in, out) is nondet.
:- mode all_sums(in, in, out) is cc_nondet.
:- mode all_sums(in, out, in) is nondet.
:- mode all_sums(in, out, in) is cc_nondet.
:- mode all_sums(in, out, out) is nondet.
:- mode all_sums(in, out, out) is cc_nondet.
:- mode all_sums(out, in, in) is nondet.
:- mode all_sums(out, in, in) is cc_nondet.
:- mode all_sums(out, in, out) is nondet.
:- mode all_sums(out, in, out) is cc_nondet.
:- mode all_sums(out, out, in) is nondet.
:- mode all_sums(out, out, in) is cc_nondet.
:- mode all_sums(out, out, out) is nondet.
:- mode all_sums(out, out, out) is cc_nondet.

% XXX we can't use partial instantiation (limitation of current mercury compiler implementation)
%% so we can't use the cryptharithm type with free holes in 'all_sums' predicate
:- pred all_sums({int, int, int}).
:- mode all_sums(out) is nondet.

:- implementation.

:- import_module int, io, list, solutions.

:- mutable(cryptarithms, list({int, int, int}), init_cryptarithms, ground, [constant]).

:- func init_cryptarithms = list({int, int, int}).
init_cryptarithms = Cryptarithms :-
    trace [
        io(!TraceIO)
    ] (
        io.write_string("Generating all cryptarithm solutions...", !TraceIO)
    ),
    % TODO try to use cc_multi do_while all_sum solutions, up to max 100
    solutions.solutions(all_sums, Cryptarithms),
    trace [
        io(!TraceIO)
    ] (
        io.write_string("\nFinished generating all cryptarithm solutions.\n", !TraceIO)
    ).

cryptarithms = Cryptarithms :- get_cryptarithms(Cryptarithms).

all_sums({X,Y,Z}) :-
    %all_sums(X, Y, Z).

    all_digits(X3, X2, X1, Y3, Y2, Y1, Z3, Z2, Z1),
    X = 100*X3 + 10*X2 + X1,
    Y = 100*Y3 + 10*Y2 + Y1,
    Z = 100*Z3 + 10*Z2 + Z1.

all_digits(X3, X2, X1, Y3, Y2, Y1, Z3, Z2, Z1) :-
    Digits = 1 .. 9,
    Chars = [X3, X2, X1, Y3, Y2, Y1, Z3, Z2, Z1],
    list.member(X3, Digits),
    list.member(X2, Digits),
    list.member(X1, Digits),
    list.member(Y3, Digits),
    list.member(Y2, Digits),
    list.member(Y1, Digits),
    list.member(Z3, Digits),
    list.member(Z2, Digits),
    list.member(Z1, Digits),
    X3 < Y3, X3 < Z3, Y3 < Z3, X3 =< 5, % remove symmetrical solutions
    list.remove_dups(Chars, Chars), % == no dupes
    Carry0 = 0,
    Sum1 = X1 + Y1 + Carry0, Z1 = Sum1 mod 10, Carry1 = Sum1 / 10,
    Sum2 = X2 + Y2 + Carry1, Z2 = Sum2 mod 10, Carry2 = Sum2 / 10,
    Sum3 = X3 + Y3 + Carry2, Z3 = Sum3 mod 10, Carry3 = Sum3 / 10,
    Carry3 = 0.

all_sums(X, Y, Z) :-
    Digits = 1 .. 9,
    Chars = [X3, X2, X1, Y3, Y2, Y1, Z3, Z2, Z1],
    list.member(X3, Digits),
    list.member(X2, Digits),
    list.member(X1, Digits),
    list.member(Y3, Digits),
    list.member(Y2, Digits),
    list.member(Y1, Digits),
    list.member(Z3, Digits),
    list.member(Z2, Digits),
    list.member(Z1, Digits),
    X3 < Y3, X3 < Z3, Y3 < Z3, X3 =< 5, % remove symmetrical solutions
    list.remove_dups(Chars, Chars), % == no dupes,
    X = 100*X3 + 10*X2 + X1,
    Y = 100*Y3 + 10*Y2 + Y1,
    Z = 100*Z3 + 10*Z2 + Z1,
    X < Y, X < Z, Y < Z, % remove symmetrical solutions
    X + Y = Z.
