:- module cryptarithm_view.

:- interface.

:- import_module globals.

:- type cryptarithm_view ---> cryptarithm_view(int, int).

:- type cryptarithm_view_event
--->    submit(int, int)
;       cancel.

:- instance view(cryptarithm_view, cryptarithm_view_event).

:- implementation.

:- import_module list, int, string.

:- instance view(cryptarithm_view, cryptarithm_view_event) where [
    (view_initial( cryptarithm_view(0, 0) )),
    (view_output(View, ViewLines) :-
        View = cryptarithm_view(Z, FailCount),
        MainLines = [
            view_line_outer,
            view_line_left_content("Solve the cryptarithm puzzle:"),
            view_line_right_content("X+Y=Z. X < Y. Each digit (1-9) must exactly occur once in all numbers."),
            view_line_empty,
            view_line_labelled_content(" X", "???"),
            view_line_labelled_content("+Y", "???"),
            view_line_labelled_content("=Z", int_to_string(Z)),
            view_line_empty,
            view_line_right_content("Enter 's[X] [Y]' to submit your answer."),
            view_line_right_content("Enter 'c' to cancel and return to the todo list."),
            view_line_outer
        ],
        (if (FailCount > 0) then
            NotificationLines = [
                view_line_outer,
                view_line_left_content("Previous answer was incorrect! Please try again or cancel."),
                view_line_outer
            ]
        else
            NotificationLines = []
        ),
        ViewLines = list.append(MainLines, NotificationLines)
    ),
    (view_input(View, Input, Events) :-
        View = cryptarithm_view(_, _),
        some [!Events] (
            !:Events = [],
            % CANCEL
            (if string.first_char(Input, ('c'), _) then
                !:Events = [ cancel | !.Events ]
            else
                true
            ),
            % SUBMIT
            (if string.first_char(Input, ('s'), Rest) then
                Words = string.words(Rest),
                % TODO error handling for out-of-bounds-index and non-integer strings
                GuessedX = string.det_to_int(list.det_index0(Words, 0)),
                GuessedY = string.det_to_int(list.det_index0(Words, 1)),
                !:Events = [ submit(GuessedX, GuessedY) | !.Events ]
            else
                true
            ),
            Events = !.Events
        )
    )
].
