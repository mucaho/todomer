:- module globals.

:- interface.

:- import_module store_state.
:- import_module thread.
:- import_module thread.channel.
:- import_module thread.mvar.
:- import_module io.
:- import_module list.
:- import_module bool.
:- import_module int.

:- typeclass action(T) where [
    func name(T) = string
].
:- type some_action ---> some[A] some_action(A) => action(A).

:- typeclass reducer_action(T) <= action(T) where [
    pred reduce(T::in, state(S)::in, state(S)::out) is det
].
:- type some_reducer_action ---> some[A] some_reducer_action(A) => reducer_action(A).

:- typeclass store_component(C, V, E) <= (view(V, E), (V -> E)) where [
    pred store_component_initial_mvar(mvar({C, V})::out, io::di, io::uo) is det,
    pred store_component_output(mvar(state(S))::in, channel(some_reducer_action)::in, mvar({C, V})::in, io::di, io::uo) is det,
    pred store_component_input(channel(some_reducer_action)::in, mvar({C, V})::in, io::di, io::uo) is det
].
:- type some_store_component ---> some[C, V, E] some_store_component(C, V) => store_component(C, V, E).

:- pred store_component_output_loop(mvar(state(S))::in, channel(some_reducer_action)::in, mvar({C, V})::in, io::di, io::uo) is det <= (store_component(C, V, E)).

:- pred store_component_input_loop(channel(some_reducer_action)::in, mvar({C, V})::in, io::di, io::uo) is det <= (store_component(C, V, E)).

:- typeclass component(C, O, V, E) <= ((C -> O), view(V, E), (V -> E)) where [
    pred component_initial_mvar(mvar({C, V})::out, io::di, io::uo) is det,
    pred component_output(mvar({C, V})::in, io::di, io::uo) is det,
    pred component_input(list(O)::out, mvar({C, V})::in, io::di, io::uo) is det
].
:- type some_component ---> some[C, O, V, E] some_component(C, V) => component(C, O, V, E).

:- typeclass view(V, E) <= (V -> E) where [
    pred view_initial(V::out) is det,
    pred view_output(V::in, list(string)::out) is det,
    pred view_input(V::in, string::in, list(E)::out) is det
].
:- type some_view ---> some[V, E] some_view(V, E) => view(V, E).

:- pred thread_yield_repeat(int::in, io::di, io::uo) is det.

:- func view_line_outer = string.
:- func view_line_empty = string.
:- func view_line_left_content(string) = string.
:- func view_line_right_content(string) = string.
:- func view_line_labelled_content(string, string) = string.

:- func bool_to_string(bool) = string.

:- pred write_lines(list(string)::in, io::di, io::uo) is det.

:- impure func impure_random(int) = int.

:- implementation.

:- import_module thread, int, random, string.

:- mutable(rsupply, random.supply, init_rsupply, ground, [untrailed]).

:- func init_rsupply = random.supply.
init_rsupply = RSupply :- random.init(0, RSupply).

impure_random(RandMax) = Random :- some [!RSupply] (
    semipure get_rsupply(!:RSupply),
    random.random(0, RandMax, Random, !RSupply),
    impure set_rsupply(!.RSupply)
).

thread_yield_repeat(N, !IO) :-
    (if N > 0 then
        thread.yield(!IO),
        thread_yield_repeat(N - 1, !IO)
    else
        true
    ).

view_line_outer = "********************************************************************************\n".
view_line_empty = "*                                                                              *\n".

view_line_left_content(Content) = Line :- string.format("* %-76s *\n", [s(Content)], Line).
view_line_right_content(Content) = Line :- string.format("* %76s *\n", [s(Content)], Line).

view_line_labelled_content(Label, Content) = Line :-
    string.format("* %-28s: %46s *\n", [s(Label), s(Content)], Line).

bool_to_string(Truth) = Repr :- if Truth = yes then Repr = "yes" else Repr = "no".

write_lines(Lines, !IO) :-
    Acc = (func(Line, AccLines) = string.append(AccLines, Line)),
    String = list.foldl(Acc, Lines, ""),
    io.write_string("\033[2J", !IO), % clear screen
    io.write_string(String, !IO).


store_component_output_loop(StateMVarComponent, ActionChannel, ComponentMVar, !IO) :-
    thread.yield(!IO),
    store_component_output(StateMVarComponent, ActionChannel, ComponentMVar, !IO),
    store_component_output_loop(StateMVarComponent, ActionChannel, ComponentMVar, !IO).

store_component_input_loop(ActionChannel, ComponentMVar, !IO) :-
    thread_yield_repeat(10, !IO), % emulate sleep by yielding multiple times
    store_component_input(ActionChannel, ComponentMVar, !IO),
    store_component_input_loop(ActionChannel, ComponentMVar, !IO).
