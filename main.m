:- module main.

:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is cc_multi.

:- implementation.

:- pragma require_feature_set([ concurrency ]).

:- import_module globals.
:- import_module store_thread.
:- import_module store_state.
:- import_module todo_component.
:- import_module todo_view.
:- import_module thread.
:- import_module thread.mvar.
:- import_module thread.channel.
:- import_module list.

% TODO
% persist state to file, effects action -> 0..n actions
% functionality to rewind state
% have the component also receive actions through ActionChannel, so that it can dispatch its own actions
% make reducer actions in-out preds so you can query actions required to get to desired state
% memoization tables on view update predicates

main(!IO) :-
    init_store_mvar(StateMVar, !IO),
    mvar.read(StateMVar, InitialState, !IO),
    init_action_channel(ActionChannel, !IO),

    StateThread = (pred(IO0::di, IO1::uo) is cc_multi :- execute_store_thread(ActionChannel, StateMVar, InitialState, IO0, IO1)),
    thread.spawn(StateThread, !IO),

    % components
    mvar.init(StateMVarTodo, !IO),
    spawn_component_thread(StateMVarTodo, ActionChannel, _: mvar({todo_component, todo_view}), !IO),

    StateMVarComponents = [
        StateMVarTodo
    ],

    MultiplexStateToComponents = (pred(IO0::di, IO1::uo) is cc_multi :- multiplex_state_to_components(StateMVar, StateMVarComponents, IO0, IO1)),
    thread.spawn(MultiplexStateToComponents, !IO).

:- pred multiplex_state_to_components(mvar(state(S))::in, list(mvar(state(S)))::in, io::di, io::uo) is det.

multiplex_state_to_components(StateMVar, StateMVarComponents, !IO) :-
        thread.yield(!IO),
        mvar.take(StateMVar, State, !IO),
        PutStateMVarComponent = (pred(StateMVarComponent::in, IO0::di, IO1::uo) is det :- mvar.put(StateMVarComponent, State, IO0, IO1)),
        list.foldl(PutStateMVarComponent, StateMVarComponents, !.IO, !:IO),
        multiplex_state_to_components(StateMVar, StateMVarComponents, !IO).

:- pred spawn_component_thread(mvar(state(S))::in, channel(some_reducer_action)::in, mvar({C, V})::out, io::di, io::uo) is cc_multi <= (store_component(C, V, E)).

spawn_component_thread(StateMVarComponent, ActionChannel, ComponentMVar, !IO) :-
    store_component_initial_mvar(ComponentMVar, !IO),
    ComponentOutput = (pred(IO0::di, IO1::uo) is cc_multi :- store_component_output_loop(StateMVarComponent, ActionChannel, ComponentMVar, IO0, IO1)),
    ComponentInput = (pred(IO0::di, IO1::uo) is cc_multi :- store_component_input_loop(ActionChannel, ComponentMVar, IO0, IO1)),
    thread.spawn(ComponentOutput, !IO),
    thread.spawn(ComponentInput, !IO).
