:- module store_state.

:- interface.

:- import_module list.
:- import_module maybe.
:- import_module bool.

:- type state(S).

:- type todo ---> todo(
    id          :: string,
    text        :: string,
    completed   :: bool
).

:- some[S] func initial_state = state(S).

:- func state(S) ^ todos = list(todo).
:- func ( state(S) ^ todos := list(todo) ) = state(S).

:- func state(S) ^ focused_todo_id = maybe(string).
:- func ( state(S) ^ focused_todo_id := maybe(string) ) = state(S).

:- pred equals_state(state(S)::in, state(S)::in) is semidet.

:- pred uuid(string::out) is det.

:- implementation.

:- import_module version_store.
:- import_module list.
:- import_module bool.
:- import_module maybe.
:- import_module int.

:- type state(S) ---> state(
    todos_var           :: mutvar(list(todo), S),
    focused_todo_id_var :: mutvar(maybe(string), S),
    store               :: version_store(S)
).

initial_state = State :-
    some [!Store] (
        !:Store = version_store.init,
        version_store.new_mutvar([], TodosVar, !Store),
        version_store.new_mutvar(no, FocusedTodoIdVar, !Store),
        State = state(TodosVar, FocusedTodoIdVar, !.Store)
    ).

state(TodosVar, _, Store) ^ todos = Todos :-
    Todos = Store ^ elem(TodosVar).

( State @ state(TodosVar, _, Store) ^ todos := NewTodos ) = NewState :-
    NewStore = (Store ^ elem(TodosVar) := NewTodos),
    NewState = (State ^ store := NewStore).

state(_, FocusedTodoIdVar, Store) ^ focused_todo_id = FocusedTodo :-
    FocusedTodo = Store ^ elem(FocusedTodoIdVar).

( State @ state(_, FocusedTodoIdVar, Store) ^ focused_todo_id := NewFocusedTodo ) = NewState :-
    NewStore = (Store ^ elem(FocusedTodoIdVar) := NewFocusedTodo),
    NewState = (State ^ store := NewStore).

equals_state(PrevState, NextState) :-
    PrevState ^ todos = NextState ^ todos,
    PrevState ^ focused_todo_id = NextState ^ focused_todo_id.

uuid(UUID) :- promise_pure semipure c_uuid(UUID).

:- pragma foreign_decl("C", "#include <stdlib.h>").
:- pragma foreign_decl("C", "char hex_alphabet[] = ""01234567789ABCDEF"";").

:- semipure pred c_uuid(string::out) is det.

:- pragma foreign_proc("C",
    c_uuid(UUID::out),
    [promise_semipure, thread_safe, will_not_call_mercury],
    "
    UUID = malloc( sizeof(char) * 36 );
    int i;
    for (i = 0; i < 36; ++i) {
        int index = rand() % 16;
        UUID[i] = hex_alphabet[index];
    }
    "
).
