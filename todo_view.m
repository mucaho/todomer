:- module todo_view.

:- interface.

:- import_module globals.
:- import_module bool.
:- import_module list.
:- import_module maybe.
:- import_module store_state.

:- type todo_view ---> todo_view(
    todos :: list(todo),
    selected_todo :: maybe(todo),
    active_todo_count :: int,
    all_todos_completed :: bool,
    any_todos_completed :: bool
).

% TODO introduce intermediate view_events,
% or introduce event typeclass view_event(E, C, V) with process_event(E::in, C::in, V::in, list(action)::out, C::out, V::out)
% and have the instances defined in the store_component
% so to be able to contain arbitrarily nested events
:- type todo_view_event
--->    change_todo_text(string, string)
;       change_todo_completion(string, bool)
;       change_focused_todo(maybe(string))
;       delete_todo(string)
;       add_todo(string)
;       check_completion(bool)
;       delete_completed.

:- instance view(todo_view, todo_view_event).

:- implementation.

:- import_module globals.
:- import_module maybe.
:- import_module list.
:- import_module string.
:- import_module bool.
:- import_module int.

:- func todo_view ^ header = todo_view_header.
View ^ header = todo_view_header(Todo) :-
    MaybeTodo = View ^ selected_todo,
    (
        MaybeTodo = yes(Todo)
    ;
        MaybeTodo = no,
        Todo = todo("", "", no)
    ).

:- func todo_view ^ contents = todo_view_contents.
View ^ contents = todo_view_contents(View ^ todos).

:- func todo_view ^ footer = todo_view_footer.
View ^ footer = todo_view_footer(View ^ active_todo_count, View ^ all_todos_completed, View ^ any_todos_completed).

:- instance view(todo_view, todo_view_event) where [
    (view_initial( todo_view([], no, 0, no, no) )),
    (view_output(View, ViewLines) :-
        MaybeTodo = View ^ selected_todo,
        (
            MaybeTodo = yes(_),
            view_output(View ^ header, TopLines)
        ;
            MaybeTodo = no,
            view_output(View ^ contents, TopLines)
        ),
        view_output(View ^ footer, FooterLines),

        list.append(TopLines, FooterLines, ViewLines)
    ),
    (view_input(View, Input, Events) :-
        MaybeTodo = View ^ selected_todo,
        (
            MaybeTodo = yes(_),
            view_input(View ^ header, Input, TopEvents)
        ;
            MaybeTodo = no,
            view_input(View ^ contents, Input, TopEvents)
        ),
        view_input(View ^ footer, Input, FooterEvents),

        list.append(TopEvents, FooterEvents, Events)
    )
].

:- type todo_view_header ---> todo_view_header(
    todo
).
:- instance view(todo_view_header, todo_view_event) where [
    (view_initial( todo_view_header(todo("", "", no)) )),
    (view_output(View, ViewLines) :-
        View = todo_view_header(Todo),
        ViewLines = [
            view_line_outer,
            view_line_left_content("EDIT TODO"),
            view_line_empty,
            view_line_labelled_content("Id", Todo ^ id),
            view_line_labelled_content("Completed", bool_to_string(Todo ^ completed)),
            view_line_labelled_content("Text", Todo ^ text),
            view_line_empty,
            view_line_right_content("Enter 'c' to toggle completion state."),
            view_line_right_content("Enter 't[Text...]' to update text."),
            view_line_right_content("Enter 'o' to exit editing mode."),
            view_line_outer
        ]
    ),
    (view_input(View, Input, Events) :-
        View = todo_view_header(Todo),
        some [!Events] (
            !:Events = [],
            % CHANGE TODO TEXT
            (if string.first_char(Input, ('t'), NewText) then
                !:Events = [ change_todo_text(Todo ^ id, NewText) | !.Events ]
            else
                true
            ),
            % CHANGE TODO COMPLETION STATE
            (if string.first_char(Input, ('c'), _) then
                !:Events = [ change_todo_completion(Todo ^ id, not(Todo ^ completed)) | !.Events ]
            else
                true
            ),
            % UNFOCUS TODO
            (if string.first_char(Input, ('o'), _) then
                !:Events = [ change_focused_todo(no) | !.Events ]
            else
                true
            ),
            Events = !.Events
        )
    )
].

:- type todo_view_contents ---> todo_view_contents(
    list(todo)
).
:- instance view(todo_view_contents, todo_view_event) where [
    (view_initial( todo_view_contents([]) )),
    (view_output(View, ViewLines) :-
        View = todo_view_contents(Todos),
        Indices = 0 .. (list.length(Todos) - 1),
        MapZippedToContentLines = (
            func(Index, Todo) = ContentLines :-
                TodoViewContent = todo_view_content(Index, Todo),
                view_output(TodoViewContent, ContentLines)
        ),
        NestedBodyLines = list.map_corresponding(MapZippedToContentLines, Indices, Todos),
        BodyLines = list.condense(NestedBodyLines),
        HeaderLines = [
            view_line_outer,
            view_line_left_content("TODO LIST"),
            view_line_empty
        ],
        FooterLines = [
            view_line_empty,
            view_line_right_content("Enter 'e[N]' to enter editing mode for the Nth Todo."),
            view_line_right_content("Enter 'r[N]' to delete the Nth todo."),
            view_line_right_content("Enter 'a[Text...]' to add a new todo containing the text."),
            view_line_outer
        ],
        list.append(HeaderLines, BodyLines, TempLines),
        list.append(TempLines, FooterLines, ViewLines)
    ),
    (view_input(View, Input, Events) :-
        View = todo_view_contents(Todos),
        some [!Events] (
            !:Events = [],
            % FOCUS TODO FOR EDITING
            (if string.first_char(Input, ('e'), EditTodoIndex) then
                % TODO error handling for out-of-bounds-index
                EditTodo = list.det_index0(Todos, string.det_to_int(EditTodoIndex)),
                !:Events = [ change_focused_todo(yes(EditTodo ^ id)) | !.Events ]
            else
                true
            ),
            % REMOVE TODO
            (if string.first_char(Input, ('r'), DeleteTodoIndex) then
                % TODO error handling for out-of-bounds-index
                DeleteTodo = list.det_index0(Todos, string.det_to_int(DeleteTodoIndex)),
                !:Events = [ delete_todo(DeleteTodo ^ id) | !.Events ]
            else
                true
            ),
            % ADD TODO
            (if string.first_char(Input, ('a'), NewTodoText) then
                !:Events = [ add_todo(NewTodoText) | !.Events ]
            else
                true
            ),
            Events = !.Events
        )
    )
].

:- type todo_view_content ---> todo_view_content(
    int, todo
).
:- instance view(todo_view_content, todo_view_event) where [
    (view_initial( todo_view_content(0, todo("", "", no)) )),
    (view_output(View, ViewLines) :-
        View = todo_view_content(Index, Todo),
        ViewLines = [
            view_line_outer,
            view_line_labelled_content("Index", int_to_string(Index)),
            view_line_labelled_content("Id", Todo ^ id),
            view_line_labelled_content("Completed", bool_to_string(Todo ^ completed)),
            view_line_labelled_content("Text", Todo ^ text),
            view_line_outer
        ]
    ),
    (view_input(_, _, []))
].

:- type todo_view_footer ---> todo_view_footer(
    int,
    bool,
    bool
).
:- instance view(todo_view_footer, todo_view_event) where [
    (view_initial( todo_view_footer(0, no, no) )),
    (view_output(View, ViewLines) :-
        View = todo_view_footer(ActiveTodoCount, AllTodosCompleted, AnyTodosCompleted),
        TopLines = [
            view_line_outer,
            view_line_left_content("ADDITIONAL INFO"),
            view_line_empty,
            view_line_labelled_content("Active count", int_to_string(ActiveTodoCount)),
            view_line_labelled_content("All todos completed", bool_to_string(AllTodosCompleted)),
            view_line_empty,
            view_line_right_content("Enter 'x' to toggle completion state for all todos.")
        ],
        (if AnyTodosCompleted = yes then
            BottomLines = [
                view_line_right_content("Enter 'f' to delete all completed."),
                view_line_outer
            ]
        else
            BottomLines = [
                view_line_outer
            ]
        ),
        list.append(TopLines, BottomLines, ViewLines)
    ),
    (view_input(View, Input, Events) :-
        View = todo_view_footer(_, _, AnyTodosCompleted),
        some [!Events] (
            !:Events = [],
            % CHECK ALL
            (if string.first_char(Input, ('x'), _) then
                !:Events = [ check_completion(not(AnyTodosCompleted)) | !.Events ]
            else
                true
            ),
            % DELETE ALL CHECKED
            (if string.first_char(Input, ('f'), _), AnyTodosCompleted = yes then
                !:Events = [ delete_completed | !.Events ]
            else
                true
            ),
            Events = !.Events
        )
    )
].
