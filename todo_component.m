:- module todo_component.

:- interface.

:- import_module globals, todo_view, cryptarithm_component, cryptarithm_view.
:- import_module io, thread, thread.mvar.

:- type todo_component
--->    uninitialized
;       running
;       checking_confirmation(mvar({cryptarithm_component, cryptarithm_view}))
;       finalized.

:- instance store_component(todo_component, todo_view, todo_view_event).

:- implementation.

:- pragma require_feature_set([ concurrency ]).

:- import_module todo_view.
:- import_module store_selectors.
:- import_module store_actions.
:- import_module store_state.
:- import_module store_reducer.
:- import_module io.
:- import_module list.
:- import_module string.
:- import_module thread.
:- import_module thread.channel.
:- import_module thread.mvar.

:- instance store_component(todo_component, todo_view, todo_view_event) where [
    pred(store_component_initial_mvar/3) is todo_component_initial_mvar,
    pred(store_component_output/5) is todo_component_output,
    pred(store_component_input/4) is todo_component_input
].

:- pred todo_component_initial(todo_component::out, todo_view::out) is det.

todo_component_initial(InitialComponentState, InitialViewState) :-
    InitialComponentState = uninitialized,
    view_initial(InitialViewState).

:- pred todo_component_initial_mvar(mvar({todo_component, todo_view})::out, io::di, io::uo) is det.

todo_component_initial_mvar(ComponentStateMVar, !IO) :-
    todo_component_initial(InitialComponentState, InitialViewState),
    InitialState = {InitialComponentState, InitialViewState},
    mvar.init(ComponentStateMVar, !IO),
    mvar.put(ComponentStateMVar, InitialState, !IO).

:- pred todo_component_output(mvar(state(S))::in, channel(some_reducer_action)::in, mvar({todo_component, todo_view})::in, io::di, io::uo) is det.

todo_component_output(StateMVar, ActionChannel, ComponentStateMVar, !IO) :-
    some [!Component, !View] (
        % wait for store state to be updated
        mvar.take(StateMVar, State, !IO),
        % take previous component state
        mvar.take(ComponentStateMVar, {!:Component, !:View}, !IO),

        ( % uninitialized
            !.Component = uninitialized,
            % TODO dispatch action to fetch state from disk instead
            channel.put(ActionChannel, 'new some_reducer_action'( refresh_component ), !IO),
            !:Component = running
        ; % running
            !.Component = running,
            % update view output
            !:View = todo_view(
                State ^ todo_list,
                State ^ selected_todo,
                State ^ active_todo_count,
                State ^ all_todos_completed,
                State ^ any_todos_completed
            ),
            view_output(!.View, ViewLines),
            write_lines(ViewLines, !IO)
        ; % checking_confirmation
            !.Component = checking_confirmation(CryptarithmComponentMVar),

            % call cryparithm component output
            component_output(CryptarithmComponentMVar, !IO)
        ; % finalized
            !.Component = finalized
            % TODO put new component state finished, so that other recursion stops too
        ),

        % publish new component state
        mvar.put(ComponentStateMVar, {!.Component, !.View}, !IO)
    ).

:- pred todo_component_input(channel(some_reducer_action)::in, mvar({todo_component, todo_view})::in, io::di, io::uo) is det.

todo_component_input(ActionChannel, ComponentStateMVar, !IO) :-
    some [!Component, !View] (
        % wait for user to input commands
        io.read_line_as_string(Result, !IO),
        % take previous component state
        mvar.take(ComponentStateMVar, {!:Component, !:View}, !IO),

        (
            Result = ok(Line),
            ( % uninitialized
                !.Component = uninitialized
            ; % running
                !.Component = running,

                % update view input
                view_input(!.View, string.strip(Line), Events),

                % publish events as store actions
                EventToActionsMap = (func(Event) = Actions :-
                    (
                        Event = change_todo_text(Id, NewText),
                        Actions = [
                            'new some_reducer_action'( update_todo_text(Id, NewText) )
                        ]
                    ;
                        Event = change_todo_completion(Id, NewCompletion),
                        Actions = [
                            'new some_reducer_action'( update_todo_completed(Id, NewCompletion) )
                        ]
                    ;
                        Event = change_focused_todo(MaybeId),
                        Actions = [
                            'new some_reducer_action'( focus_todo(MaybeId) )
                        ]
                    ;
                        Event = delete_todo(Id),
                        Actions = [
                            'new some_reducer_action'( delete_todo(Id) )
                        ]
                    ;
                        Event = add_todo(NewText),
                        Actions = [
                            'new some_reducer_action'( create_todo(NewText) )
                        ]
                    ;
                        Event = check_completion(NewCompletion),
                        Actions = [
                            'new some_reducer_action'( udpate_all_completed(NewCompletion) )
                        ]
                    ;
                        Event = delete_completed,
                        Actions = [
                            % TODO figure out a better way to retrigger component output, than to send noop action
                            'new some_reducer_action'( refresh_component )
                        ]
                    )
                ),
                NestedActions = list.map(EventToActionsMap, Events),
                FlattenedActions = list.condense(NestedActions),
                dispatchActionsToChannel(FlattenedActions, ActionChannel, !IO),

                % update component state from events
                EventToNewComponentMap = (pred(Event::in, CurrentComponentState::in, NextComponentState::out, IO0::di, IO1::uo) is det :-
                    (
                        Event = change_todo_text(_, _),
                        NextComponentState = CurrentComponentState,
                        IO1 = IO0
                    ;
                        Event = change_todo_completion(_, _),
                        NextComponentState = CurrentComponentState,
                        IO1 = IO0
                    ;
                        Event = change_focused_todo(_),
                        NextComponentState = CurrentComponentState,
                        IO1 = IO0
                    ;
                        Event = delete_todo(_),
                        NextComponentState = CurrentComponentState,
                        IO1 = IO0
                    ;
                        Event = add_todo(_),
                        NextComponentState = CurrentComponentState,
                        IO1 = IO0
                    ;
                        Event = check_completion(_),
                        NextComponentState = CurrentComponentState,
                        IO1 = IO0
                    ;
                        Event = delete_completed,
                        component_initial_mvar(CryptarithmComponentMVar, IO0, IO1),
                        NextComponentState = checking_confirmation(CryptarithmComponentMVar)
                    )
                ),
                list.foldl2(EventToNewComponentMap, Events, !Component, !IO)
            ; % checking_confirmation
                !.Component = checking_confirmation(CryptarithmComponentMVar),

                % set read line as input
                mvar.take(CryptarithmComponentMVar, {CryptarithmComponent0, CryptarithmView}, !IO),
                CryptarithmComponent1 = (CryptarithmComponent0 ^ input_line := Line),
                mvar.put(CryptarithmComponentMVar, {CryptarithmComponent1, CryptarithmView}, !IO),

                % call cryparithm component input
                component_input(CryptarithmEvents, CryptarithmComponentMVar, !IO),

                % publish events as store actions
                EventToActionsMap = (func(Event) = Actions :-
                    (
                        Event = completed,
                        Actions = [
                            'new some_reducer_action'( clear_all_completed )
                        ]
                    ;
                        Event = failed,
                        Actions = [
                            % TODO figure out a better way to retrigger component output, than to send noop action
                            'new some_reducer_action'( refresh_component )
                        ]
                    ;
                        Event = aborted,
                        Actions = [
                            % TODO figure out a better way to retrigger component output, than to send noop action
                            'new some_reducer_action'( refresh_component )
                        ]
                    )
                ),
                NestedActions = list.map(EventToActionsMap, CryptarithmEvents),
                FlattenedActions = list.condense(NestedActions),
                dispatchActionsToChannel(FlattenedActions, ActionChannel, !IO),

                % update component state from events
                EventToNewComponentMap = (pred(Event::in, CurrentComponentState::in, NextComponentState::out) is det :-
                    (
                        Event = completed,
                        NextComponentState = running
                    ;
                        Event = failed,
                        NextComponentState = CurrentComponentState
                    ;
                        Event = aborted,
                        NextComponentState = running
                    )
                ),
                list.foldl(EventToNewComponentMap, CryptarithmEvents, !Component)
            ; % finalized
                !.Component = finalized
                % TODO put new component state finished, so that other recursion stops too
            )
        ;
            % end program
            Result = eof,
            !:Component = finalized
            % TODO put new component state finished, so that other recursion stops too
        ;
            % end program
            Result = error(Error),
            io.write_string("Error: ", !IO),
            io.write_string(error_message(Error), !IO),
            io.nl(!IO),
            !:Component = finalized
            % TODO put new component state finished, so that other recursion stops too
        ),

        % publish new component state
        mvar.put(ComponentStateMVar, {!.Component, !.View}, !IO)
    ).

:- pred dispatchActionsToChannel(list(some_reducer_action)::in, channel(some_reducer_action)::in, io::di, io::uo) is det.

dispatchActionsToChannel([], _, !IO).

dispatchActionsToChannel([Action | Rest], ActionChannel, !IO) :-
    channel.put(ActionChannel, Action, !IO),
    dispatchActionsToChannel(Rest, ActionChannel, !IO).
