:- module store_actions.

:- interface.

:- import_module globals.
:- import_module maybe.
:- import_module bool.

:- type create_todo ---> create_todo(string).
:- instance action(create_todo).

:- type delete_todo ---> delete_todo(string).
:- instance action(delete_todo).

:- type focus_todo ---> focus_todo(maybe(string)).
:- instance action(focus_todo).

:- type update_todo_text ---> update_todo_text(string, string).
:- instance action(update_todo_text).

:- type update_todo_completed ---> update_todo_completed(string, bool).
:- instance action(update_todo_completed).

:- type udpate_all_completed ---> udpate_all_completed(bool).
:- instance action(udpate_all_completed).

:- type clear_all_completed ---> clear_all_completed.
:- instance action(clear_all_completed).

:- type refresh_component ---> refresh_component.
:- instance action(refresh_component).

:- implementation.

:- instance action(create_todo) where [
    name(_) = "Create Todo"
].

:- instance action(delete_todo) where [
    name(_) = "Remove Todo"
].

:- instance action(focus_todo) where [
    name(_) = "Focus Todo"
].

:- instance action(update_todo_text) where [
    name(_) = "Update Todo Text"
].

:- instance action(update_todo_completed) where [
    name(_) = "Update Todo Completed"
].

:- instance action(udpate_all_completed) where [
    name(_) = "Update All Completed"
].

:- instance action(clear_all_completed) where [
    name(_) = "Clear All Completed"
].

:- instance action(refresh_component) where [
    name(_) = "Refresh Component"
].
