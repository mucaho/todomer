# todoMer

TodoMVC in Mercury.

Written in the process of CS course: Seminar in Semantic Systems.

## Description

The purpose of this demo application is to see what developing applications in Mercury is like, which benefits or obstacles one might encounter.

TodoMer is based on [TodoMVC's app specification](https://github.com/tastejs/todomvc/blob/master/app-spec.md). That is a requirements document that describes an application for managing digital post-it notes.
Users of this application can show their list of todos, where each item has an ID, completion state and a description. The list can be changed by adding new todos, marking specific ones or all of them as (in)complete and by removing specific or already completed entries. The application also shows the count of already completed items in addition to the list. The list view is changed to an item detail view when the user edits an existing item.
TodoMVC's original app specification includes the requirement for the current state of todos be persisted and restored once the user restarts the application. To make things more interesting the original specification is extended for the purpose of this demo. The todo application should offer undo and redo, so that the user may *rewind* or *fast-forward* his actions. Additionally, before all completed todos may be deleted, a confirmation dialog needs to be filled with a captcha or solution to the posted puzzle (The additional mental capacity required to confirm your choice will prevent accidental deletion of todos).

The TodoMVC specification is written for demonstrating frameworks for single-page web applications. Its [comparison page](http://todomvc.com/) shows code examples of how this todo application can be implemented in different frameworks, so that developers can make a more informed choice of which web framework to pick for their next application. This demo application is developed as a console application instead.

## Running

* [Download and install](https://www.mercurylang.org/download.html) the mercury compiler. The demo code has been developed using v14.01.1. (At the time of writing I could not install the compiler on Win10 and used a linux distro instead).
* [Download](https://bitbucket.org/mucaho/todomer/downloads/) and extract TodoMer's source code.
* With the mercury compiler installed and in path environment, run `mmc --make -E main` to compile the application.
* After successful compilation, invoke `./main` to run the application.
* At initial startup it might take a few minutes to compute all possible arithmetic puzzles. Stand-by and press "Enter" once the console application prints out the computation success message.
* Follow the on-screen prompts to interact with the application, e.g. type in `aBuy Bread` to add a new digital post-it note.

## Misc
```
Copyright (c) 2019 mucaho (https://bitbucket.org/mucaho).
This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
```
